package com.littlebytesofpi.linphonesip.compatibility;


public interface CompatibilityScaleGestureListener {
	public boolean onScale(CompatibilityScaleGestureDetector detector);
}