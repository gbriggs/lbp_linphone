SRCPATH=.
prefix=/usr/local
exec_prefix=${prefix}
bindir=${exec_prefix}/bin
libdir=${exec_prefix}/lib
includedir=${prefix}/include
ARCH=X86
SYS=LINUX
CC=/Applications/adt-bundle-mac-x86_64/ndk/toolchains/x86-4.4.3/prebuilt/darwin-x86_64/bin/i686-linux-android-gcc
CFLAGS=-Wshadow -O3 -ffast-math -m32  -Wall -I. -I$(SRCPATH) --sysroot=/Applications/adt-bundle-mac-x86_64/ndk/platforms/android-18/arch-x86 -march=i686 -mfpmath=sse -msse -std=gnu99 -mpreferred-stack-boundary=5 -fomit-frame-pointer -fno-tree-vectorize
DEPMM=-MM -g0
DEPMT=-MT
LD=/Applications/adt-bundle-mac-x86_64/ndk/toolchains/x86-4.4.3/prebuilt/darwin-x86_64/bin/i686-linux-android-gcc -o 
LDFLAGS=-m32  --sysroot=/Applications/adt-bundle-mac-x86_64/ndk/platforms/android-18/arch-x86 -lm -ldl
LIBX264=libx264.a
AR=/Applications/adt-bundle-mac-x86_64/ndk/toolchains/x86-4.4.3/prebuilt/darwin-x86_64/bin/i686-linux-android-ar rc 
RANLIB=/Applications/adt-bundle-mac-x86_64/ndk/toolchains/x86-4.4.3/prebuilt/darwin-x86_64/bin/i686-linux-android-ranlib
STRIP=/Applications/adt-bundle-mac-x86_64/ndk/toolchains/x86-4.4.3/prebuilt/darwin-x86_64/bin/i686-linux-android-strip
AS=yasm
ASFLAGS= -O2 -f elf -DHAVE_ALIGNED_STACK=1 -DHIGH_BIT_DEPTH=0 -DBIT_DEPTH=8
RC=
RCFLAGS=
EXE=
HAVE_GETOPT_LONG=1
DEVNULL=/dev/null
PROF_GEN_CC=-fprofile-generate
PROF_GEN_LD=-fprofile-generate
PROF_USE_CC=-fprofile-use
PROF_USE_LD=-fprofile-use
HAVE_OPENCL=yes
default: cli
install: install-cli
LDFLAGSCLI = -ldl 
CLI_LIBX264 = $(LIBX264)
